import React, { Component } from 'React';
import { ScrollView, StyleSheet, Alert, View, Text, Image} from 'react-native';
import { Input, Icon, Button, ThemeProvider } from 'react-native-elements';

class Main extends Component{

    constructor(props){
        super(props);

        this.state = {
            montante: '',
            incremento: '',
            horasDia: '',
            diasSemana: '',
            result: '',
            qtdSemana: 4
        }
    }
    
    resetForm(){
        console.log('Done');
    }

    async hundleSubmit(){

        if(this.verifyTypesInput()){
            console.log('start calculating...')
            await this.calcResult();
            console.log(this.state.result);
            Alert.alert(
                'Result',
                'Your price per Hour is: $ ' + this.state.result,
                [
                    { text: 'Ok', onPress: () => this.resetForm() }
                ],
                { cancelable: false }
            );
        }
    }

    verifyTypesInput(){

        let montante = this.state.montante;
        let incremento = this.state.incremento;
        let hrsDia = this.state.horasDia;
        let diasSemana = this.state.diasSemana;

        if(isNaN(montante) || montante == '' || montante == null){
            this.showMessageError('desired salary');
            return false;
        }else if(isNaN(incremento) || incremento == ''){
            this.showMessageError('extra expenses');
            return false;
        }else if(isNaN(hrsDia) || hrsDia == ''){
            this.showMessageError('hours per day');
            return false;
        }else if(isNaN(diasSemana) || diasSemana == ''){
            this.showMessageError('number days in the week');
            return false;
        }else{
            return true;
        }
    }

    showMessageError(field){
        Alert.alert(
            'Warning',
            'The '+ field +' must be a number!',
            [
                { text: 'Ok', onPress: () => console.log('readed') }
            ],
            { cancelable: false }
        );
    }

    calcResult(){

        let montante = parseFloat(this.state.montante);
        let incremento = parseFloat(this.state.incremento);
        let horasDia = parseFloat(this.state.horasDia);
        let diasSemana = parseFloat(this.state.diasSemana);
        let qtdSemana = parseFloat(this.state.qtdSemana);

        const result = (montante * incremento) / ((horasDia * diasSemana) * qtdSemana);

        console.log(JSON.stringify(this.state));

        this.setState({
            result: result
        });
    }

    render(){
        return(
            <ScrollView style={styles.father}>
                <View style={styles.container}>
                    <Image 
                        style={styles.logo}
                        source={require('../assets/images/uPrecify.png')}
                    />
                    <Input
                        containerStyle={styles.inputStyle}
                        placeholder="Your Desired Salary"
                        leftIcon={{ type: 'font-awesome', name: 'money', color: 'white', marginRight: 5 }}
                        onChangeText={(value) => this.setState({montante: value})}
                        value={this.state.montante}
                        />
                    <Input
                        containerStyle={styles.inputStyle}
                        placeholder="Extra Expenses"
                        leftIcon={{ type: 'font-awesome', name: 'calculator', color: 'white', marginRight: 5 }}
                        onChangeText={(value) => this.setState({incremento: value})}
                        value={this.state.incremento}
                        />
                    <Input
                        containerStyle={styles.inputStyle}
                        placeholder="Hours per day"
                        leftIcon={{ type: 'font-awesome', name: 'clock-o', color: 'white', marginRight: 5 }}
                        onChangeText={(value) => this.setState({horasDia: value})}
                        value={this.state.horasDia}
                        />
                    <Input
                        containerStyle={styles.inputStyle}
                        placeholder="Number days in the week"
                        leftIcon={{ type: 'font-awesome', name: 'calendar-o', color: 'white', marginRight: 5 }}
                        onChangeText={(value) => this.setState({diasSemana: value})}
                        value={this.state.diasSemana}
                        />
                    <Button
                        onPress={() => this.hundleSubmit()}
                        title="CALCULATE"
                        raised
                        type="outline"
                        icon={
                            <Icon
                                name='gg'
                                type='font-awesome'            
                                size={24}
                                color= '#292d29'
                            />
                        }
                        buttonStyle={{
                            backgroundColor: "#fceaea"
                        }}
                        titleStyle= {{
                            color: '#292d29',
                        }}
                        containerStyle={{marginTop: 20, width: 200, alignSelf: 'center'}}
                        />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    father:{
        flex: 1,
        backgroundColor: '#178c5f'
    },
    container:{
       flex: 1,
       justifyContent: 'center',
       marginTop: 20
   },
   logo:{
       width: 160,
       height: 120,
       alignSelf: 'center'
   },
   inputStyle:{
       marginTop: 20
   }
});

export default Main;